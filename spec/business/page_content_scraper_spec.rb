require 'rails_helper'

describe PageContentScraper, type: :model do
  describe 'validations' do
    it { should validate_presence_of :url }

    it { should allow_value('http://google.com').for(:url) }
    it { should allow_value('https://google.com').for(:url) }
    it { should_not allow_value('google').for(:url) }

    it 'at least one link' do
      subject = described_class.new(url: 'http://google.com')

      html = '<html><body></body></html>'

      expect(subject).to receive(:page_body).and_return(html)

      subject.valid?

      expect(subject.errors[:base]).to include 'Must have at least one link'
    end
  end

  describe '#scrap!' do
    let(:url) { 'https://github.com/brunoarueira/fish-scripts' }

    it 'scrap the page that is informed' do
      subject = described_class.new(url: url)

      expect {
        subject.scrap!
      }.to change { PageContent.count }.by(1)

      page_content = subject.page_content

      expect(page_content.url).to eq url

      header_ones = page_content.header_contents.where(header_type: HeaderType::ONE)

      expect(header_ones.count).to eq 2

      expect(header_ones.first.content).to eq "brunoarueira/fish-scripts"
      expect(header_ones.last.content).to eq 'Fish scripts'

      # split on ; to easier test
      links = page_content.links.split(';')

      expect(links[0]).to eq 'https://help.github.com: Support'
      expect(links[1]).to eq 'https://github.com/search: Search GitHub'
      expect(links[2]).to eq 'http://fishshell.com/: fish shell'
      expect(links[3]).to eq 'https://github.com/brunoarueira/fish-scripts/fork: https://github.com/brunoarueira/fish-scripts/fork'
      expect(links[4]).to eq 'https://github.com/contact: Contact GitHub'
      expect(links[5]).to eq 'https://developer.github.com: API'
      expect(links[6]).to eq 'https://training.github.com: Training'
      expect(links[7]).to eq 'https://shop.github.com: Shop'
      expect(links[8]).to eq 'https://github.com/blog: Blog'
      expect(links[9]).to eq 'https://github.com/about: About'
      expect(links[10]).to eq 'https://github.com/site/terms: Terms'
      expect(links[11]).to eq 'https://github.com/site/privacy: Privacy'
      expect(links[12]).to eq 'https://github.com/security: Security'
      expect(links[13]).to eq 'https://status.github.com/: Status'
      expect(links[14]).to eq 'https://help.github.com: Help'
    end
  end
end
