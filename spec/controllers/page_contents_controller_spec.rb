require 'rails_helper'

describe PageContentsController do
  describe 'GET #index' do
    let!(:page_content) { create(:page_content) }

    before do
      get :index
    end

    it { expect(response.header['Content-Type']).to include 'application/json' }

    it 'return the page contents stored' do
      body_first = JSON.parse(response.body).first

      expect(body_first.keys).to eq ["url", "links", "header_contents"]

      expect(body_first.values_at("url")[0]).to eq page_content.url
      expect(body_first.values_at("links")[0]).to eq page_content.links

      headers = body_first.values_at("header_contents")

      header_content_first = page_content.header_contents.first

      expect(headers[0][0].values_at("content")[0]).to eq header_content_first.content
      expect(headers[0][0].values_at("header")[0]).to eq HeaderType.t(header_content_first.header_type)

      header_content_second = page_content.header_contents.find_by(id: header_content_first.id + 1)

      expect(headers[0][1].values_at("content")[0]).to eq header_content_second.content
      expect(headers[0][1].values_at("header")[0]).to eq HeaderType.t(header_content_second.header_type)

      header_content_third = page_content.header_contents.find_by(id: header_content_second.id + 1)

      expect(headers[0][2].values_at("content")[0]).to eq header_content_third.content
      expect(headers[0][2].values_at("header")[0]).to eq HeaderType.t(header_content_third.header_type)
    end
  end

  describe 'POST #create' do
    context 'success' do
      let(:url) { 'https://github.com/brunoarueira/fish-scripts' }

      it 'creates a page content record' do
        expect {
          post :create, params: { url: url }
        }.to change { PageContent.count }.by(1)
      end
    end

    context 'failure' do
      before do
        post :create, params: { url: '' }
      end

      it { expect(response.header['Content-Type']).to include 'application/json' }
      it { expect(response.status).to eq 422 }
      it { expect(response.body).to eq "{\"url\":[\"can't be blank\"]}" }
    end
  end
end
