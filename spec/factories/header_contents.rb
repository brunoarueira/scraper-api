FactoryGirl.define do
  factory :header_content do
    sequence(:content) { |n| "content #{n}" }
    sequence(:header_type, (1..3).cycle) { |n| n }

    association :page_content
  end
end
