FactoryGirl.define do
  factory :page_content do
    url   'https://github.com/brunoarueira/fish-scripts'
    links 'http://fishshell.com/: fish shell;https://github.com/brunoarueira/fish-scripts/fork: https://github.com/brunoarueira/fish-scripts/fork;'

    after :build do |page_content|
      page_content.header_contents = build_list(:header_content, 3, page_content: page_content)
    end
  end
end
