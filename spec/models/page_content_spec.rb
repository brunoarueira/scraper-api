require 'rails_helper'

describe PageContent do
  describe 'associations' do
    it { should have_many :header_contents }
  end

  describe 'validations' do
    it { should validate_presence_of :url }

    it { should allow_value('http://google.com').for(:url) }
    it { should allow_value('https://google.com').for(:url) }
    it { should_not allow_value('google').for(:url) }
  end
end
