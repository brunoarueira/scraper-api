require 'rails_helper'

describe HeaderContent do
  describe 'associations' do
    it { should belong_to :page_content }
  end

  describe 'validations' do
    it { should validate_presence_of :content }
    it { should validate_presence_of :page_content }
  end
end
