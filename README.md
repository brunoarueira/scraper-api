# Scraper API

This app contains a scraper api to store pages using a url informed and can be
possible to list pages stored as well.

## Endpoints

### GET /page_contents

This endpoint list the page contents stored which was previously scraped.

### POST /page_contents

Accepts the url parameter that is needed to scrap and extract headers and
links.

## Development

Firstly, You will have to clone the repository and after:

	bundle install
	rake db:create rake db:migrate
	bundle exec rails s

To test the API, You can use our friendly curl:

	// To post
	curl -H "Content-Type:application/json; charset=utf-8" -d '{"url":"<some-url>"}' http://localhost:3000/page_contents

	// To list some stored page contents
	curl -H "Content-Type:application/json; charset=utf-8" http://localhost:3000/page_contents

## Author

Bruno Arueira
