require_relative 'boot'

require "active_model/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ScraperApi
  class Application < Rails::Application
    config.autoload_paths += %W(
      #{config.root}/app/enumerations
    )

    config.api_only = true
  end
end
