Rails.application.routes.draw do
  resources :page_contents, only: [:index, :create]
end
