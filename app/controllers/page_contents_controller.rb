class PageContentsController < ApplicationController
  has_scope :page, only: [:index]
  has_scope :per,  only: [:index]

  # GET /page_contents
  def index
    @page_contents = apply_scopes(PageContent.load_dependencies)

    render json: @page_contents
  end

  # POST /page_contents
  def create
    page_content_scraper = PageContentScraper.new(page_content_params)

    if page_content_scraper.scrap!
      @page_content = page_content_scraper.page_content

      render json: @page_content, status: :created
    else
      render json: page_content_scraper.errors, status: :unprocessable_entity
    end
  end

  private

  def page_content_params
    params.permit(:url)
  end
end
