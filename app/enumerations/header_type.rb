class HeaderType < EnumerateIt::Base
  associate_values one: 1,
                   two: 2,
                   three: 3
end
