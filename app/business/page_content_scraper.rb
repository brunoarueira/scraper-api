require 'net/http'

class PageContentScraper
  include ActiveModel::Validations

  attr_accessor :url, :page_content

  validates :url, presence: true
  validates :url, format: { with: URI.regexp }, allow_blank: true

  validate :at_least_one_link

  def initialize(params = {})
    self.url = params[:url]
  end

  def scrap!
    if self.valid?
      page_content = PageContent.new(url: self.url)
      page_content.links = ''

      header_ones.each do |header_one|
        text = cleanup(header_one.text)

        page_content.header_contents.build(content: text, header_type: HeaderType::ONE)
      end

      header_twos.each do |header_two|
        text = cleanup(header_two.text)

        page_content.header_contents.build(content: text, header_type: HeaderType::TWO)
      end

      header_threes.each do |header_three|
        text = cleanup(header_three.text)

        page_content.header_contents.build(content: text, header_type: HeaderType::THREE)
      end

      links.each do |link|
        href = link.attribute('href').to_s
        text = cleanup(link.text)

        if href.present? && text.present? && is_a_valid_url?(href)
          page_content.links << "#{href}: #{text};"
        end
      end

      page_content.save

      self.page_content = page_content
    end
  end

  protected

  def at_least_one_link
    return unless url.present? && valid_url?

    if none_link?
      errors.add(:base, I18n.t(:at_least_one_link))
    end
  end

  def valid_url?
    URI.regexp =~ self.url
  end

  def page_body
    @page_body ||= Net::HTTP.get(URI.parse(self.url))
  end

  def document
    @document ||= Oga.parse_html(page_body)
  end

  def none_link?
    links.empty?
  end

  def links
    @links ||= document.xpath('//a[@href]')
  end

  def header_ones
    @header_ones ||= document.xpath('//h1')
  end

  def header_twos
    @header_twos ||= document.xpath('//h2')
  end

  def header_threes
    @header_threes ||= document.xpath('//h3')
  end

  def is_a_valid_url?(href)
    URI.regexp =~ href
  end

  def cleanup(text)
    text.gsub("\n", '').strip
  end
end
