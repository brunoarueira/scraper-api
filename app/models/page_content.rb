class PageContent < ApplicationRecord
  has_many :header_contents

  validates :url, presence: true
  validates :url, format: { with: URI.regexp }, allow_blank: true

  scope :load_dependencies, -> {
    includes(:header_contents)
  }
end
