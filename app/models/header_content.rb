class HeaderContent < ApplicationRecord
  belongs_to :page_content

  has_enumeration_for :header_type

  validates :content, :page_content, presence: true
end
