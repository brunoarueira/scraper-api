class HeaderContentSerializer < ActiveModel::Serializer
  attributes :header, :content

  belongs_to :page_content

  def header
    HeaderType.t(object.header_type)
  end
end
