class PageContentSerializer < ActiveModel::Serializer
  attributes :url, :links

  has_many :header_contents
end
