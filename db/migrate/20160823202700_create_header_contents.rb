class CreateHeaderContents < ActiveRecord::Migration[5.0]
  def change
    create_table :header_contents do |t|
      t.integer :header_type
      t.string :content
      t.references :page_content, foreign_key: true

      t.timestamps
    end
  end
end
